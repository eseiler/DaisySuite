r"""
\begin{table*}[ht]
\small
\centering
\captionsetup{width=\textwidth}
\caption{TEST FOR CANDIDATES}
\vspace{.5ex} 
\resizebox{\textwidth}{!}{%
\begin{tabu}{*{3}{c}@{\hskip 1cm}*{3}{c}@{\hskip 1cm}*{2}{c}}
\rowfont{\bfseries}
 \multicolumn{3}{c}{\hspace*{-1.15cm} \xrfill[3pt]{0.5pt} ~Candidate~\xrfill[3pt]{0.5pt}} &   \multicolumn{3}{c}{\hspace*{-1.4cm} \xrfill[3pt]{0.5pt} ~MicrobeGPS metrics~\xrfill[3pt]{0.5pt}\hspace*{-0.2cm}} &   \multicolumn{2}{c}{\hspace*{-1cm} \xrfill[3pt]{0.5pt} ~DaisyGPS metrics~\xrfill[3pt]{0.5pt}\hspace*{-0.5cm}}  \\
         Type      & Name & Accession.Version &  Number Reads      & Validity      & Heterogeneity &   Property & Property Score\\
         \midrule
         \end{tabu}
         }
         \label{tab:moo}
         \end{table*
"""


def get_header1():

    return r"""\documentclass[10pt,twocolumn,twoside]{scrartcl}
%\documentclass[twocolumn,twoside]{article}
%==============================================================================%

\usepackage[a4paper,top=2.5cm,bottom=2.5cm,left=1.5cm,right=1.5cm]{geometry}
\usepackage{graphicx}
\usepackage{array}%Benutzung der tabular-Umgebung
\usepackage{multirow}
\usepackage{tabu}
\usepackage{xcolor,colortbl}
\usepackage{booktabs}
\setlength{\columnsep}{2em}
\usepackage{makecell}
\usepackage{xhfill}

\usepackage[%
font={scriptsize,sf},
labelfont={scriptsize,bf},
format=hang,    
format=plain,
margin=0pt,
width=\textwidth,
]{caption}

\begin{document}

\begin{table*}[ht]
\small
\centering
\captionsetup{width=\textwidth}
\caption{Acceptor and donor candidates for """

def get_caption():
    return '{sample}.'

def get_header2():
    return r"""}
\vspace{.5ex} 
\resizebox{\textwidth}{!}{%
\begin{tabu}{*{3}{c}@{\hskip 1cm}*{3}{c}@{\hskip 1cm}*{2}{c}}
\rowfont{\bfseries}
\multicolumn{3}{c}{\hspace*{-1.15cm} \xrfill[3pt]{0.5pt} ~Candidate~\xrfill[3pt]{0.5pt}} &   \multicolumn{3}{c}{\hspace*{-1.4cm} \xrfill[3pt]{0.5pt} ~MicrobeGPS metrics~\xrfill[3pt]{0.5pt}\hspace*{-0.2cm}} &   \multicolumn{2}{c}{\hspace*{-1cm} \xrfill[3pt]{0.5pt} ~DaisyGPS metrics~\xrfill[3pt]{0.5pt}\hspace*{-0.5cm}}  \\
Type      & Name & Accession.Version &  Number Reads      & Validity      & Heterogeneity &   Property & Property Score\\
\midrule
"""

def get_tail():
    return r"""}
\end{table*}

\end{document}"""

def get_body1():
    return r"""\begin{table*}[ht]
\small
\centering
\captionsetup{width=\textwidth}
\caption{Acceptor and donor candidates for """

def get_body2():
    return r"""\end{tabu}
}
\label{tab:"""

def get_label():
    return '{label}'

def get_body3():
    return r"""}
\end{table*}"""
