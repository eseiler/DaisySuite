.. _install_conda:

.. |br| raw:: html

   <br />

===================
Install with Conda
===================

---------------------
Installing DaisySuite
---------------------

We recommend the usage of Conda to install DaisySuite. |br|\ If you prefer to not use Conda, you can find instructions in the :ref:`Install with git<install_git>` chapter.

To install Conda under Linux 64bit, run

.. code-block:: bash
  
  wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  chmod +x Miniconda3-latest-Linux-x86_64.sh
  ./Miniconda3-latest-Linux-x86_64.sh

and follow the instructions. |br|\ Please visit `the Miniconda homepage <https://conda.io/miniconda.html>`_ for further information.

Next, add the `bioconda <https://bioconda.github.io>`_ channel to your conda installation.

.. code-block:: bash

  conda config --add channels defaults
  conda config --add channels conda-forge
  conda config --add channels bioconda

Once Conda is available, DaisySuite can be easily installed via

.. code-block:: bash

  conda install daisysuite

Note that older or newer versions of already on your system existing tools, e.g. bwa, will be installed according to the specified requirements for DaisySuite in the Conda package. |br|\ To install DaisySuite in a new environment and thereby not altering any of your existing installations, use

.. code-block:: bash

  conda create -n daisysuite_env daisysuite

where ``-n daisysuite_env`` specifies the environment name and can be chosen freely.  |br|\  Use ``source activate daisysuite_env`` to activate your new environment and ``source deactivate`` to exit the environment.


-------------------------------
Additional dependency for Laser
-------------------------------

Laser uses bwa for structural variation analysis and requires the additional bwa perl script ``xa2multi.pl`` that is usually not installed with bwa, but is available in the `bwa github <https://raw.githubusercontent.com/lh3/bwa/master/xa2multi.pl>`_. |br|\ ``xa2multi.pl`` needs to be present in the ``$PATH`` variable in order to use Laser, for example:

.. code-block:: bash 

  mkdir ~/bin
  cd ~/bin
  wget https://raw.githubusercontent.com/lh3/bwa/master/xa2multi.pl
  export PATH=~/bin:$PATH

---------------------
Setting DaisySuite up
---------------------

You can automatically download and create all required data by running ``DaisySuite_setup <dir>``. This will put the NCBI database and corresponding indices into the directory ``<dir>``.  |br|\  Alternatively, the requirements are explained in the :ref:`Database requirements section<database>`.
Yara needs up to 1 TB of temporary disk space to create its index. Currently, the setup script will fail if one of the steps fails. In this case, consider running the individual steps one by one (see the :ref:`Database creation section <tutorial/database>`).
