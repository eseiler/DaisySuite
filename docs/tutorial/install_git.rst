.. _install_git:

.. |br| raw:: html

   <br />

================
Install with git
================

---------------------
Installing DaisySuite
---------------------

If you prefer to use Conda, you can find instructions in the :ref:`Install with Conda<install_conda>` chapter.

To use DaisySuite, the following dependencies need to be satisfied and globally available:

* `bedtools <http://bedtools.readthedocs.io/en/latest/content/installation.html>`_
* `biopython <http://biopython.org/wiki/Download>`_
* `bwa <https://sourceforge.net/projects/bio-bwa/files/>`_
* `clever-toolkit (Laser) <https://bitbucket.org/tobiasmarschall/clever-toolkit/wiki/Home#markdown-header-installation>`_
* `gustaf <http://packages.seqan.de/gustaf/>`_
* `mason2 <http://packages.seqan.de/mason2/>`_
* `pandas <http://pandas.pydata.org/getpandas.html>`_
* `pysam <http://pysam.readthedocs.io/en/latest/installation.html>`_
* `sak <http://packages.seqan.de/sak/>`_
* `samtools <http://www.htslib.org/download/>`_
* `scipy <https://www.scipy.org/install.html>`_
* `snakemake <https://snakemake.readthedocs.io/en/stable/index.html>`_
* `stellar <http://packages.seqan.de/stellar/>`_
* `yara <http://packages.seqan.de/yara/>`_

To install DaisySuite, run

.. code-block:: bash
  
  git clone https://gitlab.com/eseiler/DaisySuite.git
  cd DaisySuite
  chmod +x DaisySuite*

For easy access, you might want to add the DaisySuite directory to your PATH variable, e.g.

.. code-block:: bash

  export PATH=~/DaisySuite/:$PATH

-------------------------------
Additional dependency for Laser
-------------------------------

Laser uses bwa for structural variation analysis and requires the additional bwa perl script ``xa2multi.pl`` that is usually not installed with bwa, but is available in the `bwa github <https://raw.githubusercontent.com/lh3/bwa/master/xa2multi.pl>`_. |br|\ ``xa2multi.pl`` needs to be present in the ``$PATH`` variable in order to use Laser, for example:

.. code-block:: bash 

  mkdir ~/bin
  cd ~/bin
  wget https://raw.githubusercontent.com/lh3/bwa/master/xa2multi.pl
  export PATH=~/bin:$PATH

---------------------
Setting DaisySuite up
---------------------

You can automatically download and create all required data by running ``DaisySuite_setup <dir>``. This will put the NCBI database and corresponding indices into the directory ``<dir>``. |br| Alternatively, the requirements are explained in the :ref:`Database requirements section<database>`.
